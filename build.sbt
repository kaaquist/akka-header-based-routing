name := "akka-header-based-routing"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "com.typesafe.akka"         %         "akka-http_2.12"        % "10.0.4",
  "org.json4s"                %%        "json4s-native"         % "3.6.2",
  "org.json4s"                %%        "json4s-jackson"        % "3.6.2",
  //Logger
  "org.log4s"                 %%        "log4s"                 % "1.6.1",
  "ch.qos.logback"            %         "logback-classic"       % "1.2.3",
  "org.scalatest"             %%        "scalatest"             % "3.0.5"         % "test"
)