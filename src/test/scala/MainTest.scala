import akka.http.scaladsl.model.HttpMethods.{POST, GET}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import org.scalatest.FunSuite

import scala.collection.immutable


class MainTest extends FunSuite {
  val httpEntity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, "1.1.1")
  val httpHeaders: immutable.Seq[HttpHeader] = RawHeader("x-call-Reference", "1234") :: RawHeader("x-call-Reference", "1234") :: RawHeader("x-call-MessageType", "") :: RawHeader("x-call-CallingParty", "") :: RawHeader("x-call-CalledParty", "") :: RawHeader("x-call-FrameSize", "20") :: RawHeader("x-call-MediaFormat", "") :: RawHeader("x-call-CallDirection","") :: Nil
  val httpProtocol = HttpProtocol("HTTP/2.0")
  test("Main.mapCallMessage") {
    val testRequest = HttpRequest(POST, Uri("/call"), httpHeaders, httpEntity, httpProtocol)
    assert(Main.mapCallMessage(testRequest).reference.equals("1234"))
  }

  test("Main.main.requestHandler") {
    val testRequest = HttpRequest(GET, Uri.apply("/ping"),httpHeaders, httpEntity, httpProtocol)
    assert(Main.requestHandler(testRequest).isCompleted)
  }
}
