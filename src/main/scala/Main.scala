import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.headers.RawHeader
import org.json4s._
import org.json4s.native.JsonMethods._
import org.log4s._

import scala.concurrent.Future

/**
  * Created by kasper on 16/11/2018
  */
object Main {
  private[this] val log = getLogger
  case class CallMessage(reference: String, messageType: String, callingParty: String, calledParty: String, frameSize:	Int, mediaFormat: String, callDirection: String)
  implicit val actorSystem = ActorSystem("system")
  implicit val actorMaterializer = ActorMaterializer()
  implicit val _200Http: String = compact(render(parse(""" { "http-code": 200, "http-msg":  "success" } """)))
  implicit val _500Http: String = compact(render(parse(""" { "http-code" : 500, "http-msg": "x-call-MessageType not set" } """)))
  implicit val _501Http: String = compact(render(parse(""" { "http-code" : 500, "http-msg": "x-call-Reference not set" } """)))
  implicit val _404Http: String = compact(render(parse( """ { "http-code" : 404, "http-msg": "Unknown resource!" } """)))
  implicit val _PONG: String = compact(render(parse( """ { "http-code" : 200, "http-msg": "PONG!" } """)))



  def main(args: Array[String]) {
    Http().bindAndHandleAsync(requestHandler, "localhost", 9908)
    log.info("server started at 9908")
  }


  def requestHandler(httpRequest: HttpRequest):  Future[HttpResponse] = {
    httpRequest match {
      case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
        Future.successful(HttpResponse(200, entity = _200Http))

      case HttpRequest(GET, Uri.Path("/ping"), _, _, _) =>
        Future.successful(HttpResponse(200, entity = _PONG))

      case HttpRequest(POST, Uri.Path("/call"), _, _, _) =>
        //println(httpRequest.headers.groupBy(_.name()).mapValues(_.last))
        if (httpRequest.getHeader("x-call-MessageType").isPresent) {
          httpRequest.getHeader("x-call-MessageType").get().value().toUpperCase match {
            case "ACK" => {
              val call = mapCallMessage(httpRequest)
              if (call.reference.isEmpty) Future.successful(HttpResponse(500, entity = _501Http))
              log.info("ACK")
            }
            case "INVITE" => {
              val call = mapCallMessage(httpRequest)
              if (call.reference.isEmpty) Future.successful(HttpResponse(500, entity = _501Http))
              else log.info("INVITE" + call.toString)
            }
            case "CANCEL" => {
              val call = mapCallMessage(httpRequest)
              if (call.reference.isEmpty) Future.successful(HttpResponse(500, entity = _501Http))
              log.info("CANCEL")
            }
            case "BYE" => {
              val call = mapCallMessage(httpRequest)
              if (call.reference.isEmpty) Future.successful(HttpResponse(500, entity = _501Http))
              log.info("BYE")
            }
            case "RTP" => {
              val call = mapCallMessage(httpRequest)
              if (call.reference.isEmpty) Future.successful(HttpResponse(500, entity = _501Http))
              log.info("RTP")
            }
            case _ => log.info("Unknown header value")
          }
          Future.successful(HttpResponse(200, entity = _200Http))
        } else {
          Future.successful(HttpResponse(500, entity = _500Http))
        }


      case r: HttpRequest =>
        r.discardEntityBytes() // important to drain incoming HTTP Entity stream
        Future.successful(HttpResponse(404, entity = _404Http))
    }
  }

  def mapCallMessage(httpRequest: HttpRequest): CallMessage = {
    CallMessage(
      httpRequest.getHeader("x-call-Reference").orElseGet(() => RawHeader("x-call-Reference", null)).value(),
      httpRequest.getHeader("x-call-MessageType").orElseGet(() => RawHeader("x-call-MessageType", "not-set")).value().toUpperCase,
      httpRequest.getHeader("x-call-CallingParty").orElseGet(() => RawHeader("x-call-CallingParty", "not-set")).value(),
      httpRequest.getHeader("x-call-CalledParty").orElseGet(() => RawHeader("x-call-CalledParty", "not-set")).value(),
      httpRequest.getHeader("x-call-FrameSize").orElseGet(() => RawHeader("x-call-FrameSize", "20")).value().toInt,
      httpRequest.getHeader("x-call-MediaFormat").orElseGet(() => RawHeader("x-call-MediaFormat", "not-set")).value(),
      httpRequest.getHeader("x-call-CallDirection").orElseGet(() => RawHeader("x-call-CallDirection", "not-set")).value()
    )
  }
}
